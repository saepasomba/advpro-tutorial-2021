package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class OrderFoodTest {
    private OrderFood orderFood;
    private OrderFood order1;
    private OrderFood order2;

    @BeforeEach
    public void setUp() throws Exception {
        orderFood = OrderFood.getInstance();
        order1 = orderFood.getInstance();
        order2 = orderFood.getInstance();
    }

    @Test
    public void testOrderFoodReturnInstance() {
        assertNotNull(orderFood);
    }

    @Test
    public void testOrderDrinkMultipleInstance() {
        assertThat(order1).isEqualToComparingFieldByField(order2);
    }

    @Test
    public void testDrinkNameOrdered() {
        orderFood.setFood("dummyName");
        assertEquals("dummyName", orderFood.getFood());
        assertEquals("dummyName", orderFood.toString());
    }

}
