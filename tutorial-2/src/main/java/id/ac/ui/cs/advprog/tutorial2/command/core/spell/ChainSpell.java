package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    ArrayList<Spell> listOfSpells;
    public ChainSpell(ArrayList<Spell> listOfSpells) {
        this.listOfSpells = listOfSpells;
    }

    @Override
    public void cast() {
        for (Spell spell : listOfSpells) {
            spell.cast();
        }
    }

    @Override
    public void undo() {
        for (int i = listOfSpells.size() - 1; i >= 0; i--) {
            listOfSpells.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
