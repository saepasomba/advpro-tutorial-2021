package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Entity
@Table(name="log")
@Data
@Getter
@NoArgsConstructor
public class Log {

    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)

    private long id;

    @Column(name = "start_time")
    private LocalDateTime start;

    @Column(name = "end_time")
    private LocalDateTime end;

    @Column(name = "jam_kerja")
    private double jamKerja;

    @Column(columnDefinition = "TEXT")
    private String description;

    @ManyToOne
    @JoinColumn(name = "npm")
    private Mahasiswa mahasiswa;

    @JsonCreator
    public Log(
            @JsonProperty("start") String start,
            @JsonProperty("end") String end,
            @JsonProperty("description") String description
    ) {
        this.start = LocalDateTime.parse(start);
        this.end = LocalDateTime.parse(end);
        this.description = description;
        this.jamKerja = (this.start.until(this.end, ChronoUnit.SECONDS))/3600.0;
    }
}
