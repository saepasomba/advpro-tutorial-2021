package csui.advpro2021.tais.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.text.DateFormatSymbols;
import java.time.Duration;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SummaryLog {
    private int year;

    private String month;

    private double jamKerja;

    private double pembayaran;

    public SummaryLog(int year, int month, double jamKerja) {
        this.year = (int) year;
        this.month = new DateFormatSymbols().getMonths()[month-1];
        this.jamKerja = jamKerja;
        this.pembayaran = jamKerja * 350;
    }
}