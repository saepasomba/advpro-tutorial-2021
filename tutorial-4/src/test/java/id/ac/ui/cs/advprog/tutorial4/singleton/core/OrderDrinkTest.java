package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class OrderDrinkTest {
    private OrderDrink orderDrink;
    private OrderDrink order1;
    private OrderDrink order2;

    @BeforeEach
    public void setUp() throws Exception {
        orderDrink = OrderDrink.getInstance();
        order1 = orderDrink.getInstance();
        order2 = orderDrink.getInstance();
    }

    @Test
    public void testOrderDrinkReturnInstance() {
        assertNotNull(orderDrink);
    }

    @Test
    public void testOrderDrinkMultipleInstance() {
        assertThat(order1).isEqualToComparingFieldByField(order2);
    }

    @Test
    public void testDrinkNameOrdered() {
        orderDrink.setDrink("dummyName");
        assertEquals("dummyName", orderDrink.getDrink());
        assertEquals("dummyName", orderDrink.toString());
    }

}
