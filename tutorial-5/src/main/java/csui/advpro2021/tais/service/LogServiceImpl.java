package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.SummaryLog;
import org.springframework.beans.factory.annotation.Autowired;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import org.springframework.stereotype.Service;
import org.springframework.cglib.core.CollectionUtils;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@Service
public class LogServiceImpl implements LogService {

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private MahasiswaService mahasiswaService;

    @Override
    public Log createLog(Log log, String npm) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        log.setMahasiswa(mahasiswa);
        logRepository.save(log);
        return log;
    }

    @Override
    public Log getLog(String id) {
        return logRepository.findById(Long.parseLong(id));
    }

    @Override
    public Log updateLog(Log log, String id) {
        Log currentLog = this.getLog(id);
        log.setMahasiswa(currentLog.getMahasiswa());
        log.setId(Long.parseLong(id));
        logRepository.save(log);
        return log;
    }

    @Override
    public void deleteLog(String id) {
        logRepository.deleteById(id);
    }

    @Override
    public Iterable<Log> getListLogsByNPMMahasiswa(String npm, String month) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        List<Log> logs = logRepository.findByMahasiswa(mahasiswa);
        List<Log> result = new LinkedList<Log>();

        if (month != null) {
            for (Log log : logs) {
                if (month.equalsIgnoreCase(getMonthName(log.getStart()))) {
                    result.add(log);
                }
            }
        }
        return result;
    }

    @Override
    public Iterable<SummaryLog> getLogsSummaryByNPMMahasiswa(String npm, String month, String year) {
        List<SummaryLog> summaries = logRepository.getRawLogSummaries(npm);
        List<SummaryLog> removed = new ArrayList<SummaryLog>();

        if (month != null) {
            for (SummaryLog logSummary : summaries) {
                if (!month.equalsIgnoreCase(logSummary.getMonth())) {
                    removed.add(logSummary);
                }
            }
        }

        if (year != null) {
            for (SummaryLog logSummary : summaries) {
                if (Integer.parseInt(year) != logSummary.getYear()) {
                    removed.add(logSummary);
                }
            }
        }

        for (SummaryLog logSummary : removed) {
            summaries.remove(logSummary);
        }
        return summaries;
    }

    protected String getMonthName(LocalDateTime dateTime) {
        Month month = dateTime.getMonth();
        return month.name();
    }
}
