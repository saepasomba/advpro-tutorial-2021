Nama: Sae Pasomba<br>
NPM: 1906350862<br>
Kelas: Adpro C

## Jelaskan perbeadaan keduanya beserta keuntungan dan kekurangan masing-masing _approach_!
### Lazy Instantiation
Pada pendekatan ini, objek baru dibuat saat pertama kali dipanggil. Jika dilihat pada tutorial 4, method `getInstance()` pada `orderDrink.java` baru akan membuat objek OrderDrink saat dipanggil dan orderDrink masih `null`. Method akan me-`return` objek yang baru saja dibuat/yang sudah pernah dibuat.

**Kelebihan**<br>
- Hanya akan membuat objek-objek yang diperlukan.
- Memulai aplikasi lebih cepat.
<br>

**Kekurangan**
- Waktu proses selama runtime lebih lambat.
- Sulit untuk _multithreading_.

### Eager Instantiation
Pada pendekatan ini, objek dibuat di awal program dijalankan. Jika dilihat pada tutorial 4, method `getInstance()` pada `orderFood.java` akan langsung `return` orderFood.

**Kelebihan**<br>
- Waktu proses selama runtime lebih cepat.
- Dapat meningkatkan _multithreading_.
  <br>

**Kekurangan**
- Bisa saja membuat objek-objek yang tidak diperlukan.
- Memulai aplikasi lebih lambat.