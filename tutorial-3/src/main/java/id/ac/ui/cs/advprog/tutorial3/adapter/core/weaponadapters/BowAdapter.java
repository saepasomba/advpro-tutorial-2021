package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class BowAdapter implements Weapon {

    private Bow bow;
    private Boolean aimShot = false;

    public BowAdapter(Bow bow) {
        this.bow = bow;
    }

    @Override
    public String normalAttack() {
        return bow.shootArrow(aimShot);
    }

    @Override
    public String chargedAttack() {
        if (aimShot) {
            aimShot = false;
            return "Leaving aim shot mode";
        } else {
            aimShot = true;
            return "Entering aim shot mode";
        }
    }

    @Override
    public String getName() {
        return bow.getName();
    }

    @Override
    public String getHolderName() {
        return bow.getHolderName();
    }
}
