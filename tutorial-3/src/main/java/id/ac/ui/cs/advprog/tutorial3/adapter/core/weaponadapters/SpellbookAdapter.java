package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private Boolean largeSpellAvailable = true;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
    }

    @Override
    public String normalAttack() {
        largeSpellAvailable = true;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if (largeSpellAvailable) {
            largeSpellAvailable = false;
            return spellbook.largeSpell();
        } else {
            return "Large spell is not available!";
        }
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
