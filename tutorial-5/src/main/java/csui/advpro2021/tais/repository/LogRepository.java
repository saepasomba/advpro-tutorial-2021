package csui.advpro2021.tais.repository;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.SummaryLog;
import csui.advpro2021.tais.model.Mahasiswa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LogRepository extends JpaRepository<Log, String> {
    Log findById(long id);

    List<Log> findByMahasiswa(Mahasiswa mahasiswa);

    @Query("SELECT new csui.advpro2021.tais.model.SummaryLog( " +
            "EXTRACT (MONTH from log.start) AS month, " +
            "EXTRACT (YEAR from log.start) AS year, " +
            "SUM(log.jamKerja))" +
            "FROM Log AS log " +
            "WHERE npm = :npm " +
            "GROUP BY 1, 2")
    List<SummaryLog> getRawLogSummaries(String npm);
}