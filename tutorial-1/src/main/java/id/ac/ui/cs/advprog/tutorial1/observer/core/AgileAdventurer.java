package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {


    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        this.guild = guild;
    }

    @Override
    public void update() {
        String questType = guild.getQuestType();
        if (!questType.equals("Escort")) {
            getQuests().add(guild.getQuest());
        }
    }
}
