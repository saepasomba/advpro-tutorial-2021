package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;
    @Autowired
    private WeaponRepository weaponRepository;
    @Autowired
    private SpellbookRepository spellbookRepository;
    @Autowired
    private BowRepository bowRepository;


    @Override
    public List<Weapon> findAll() {
        List<Weapon> allWeapon = new LinkedList<Weapon>();


        for (Weapon weapon : weaponRepository.findAll()) {
            allWeapon.add(weapon);
        }

        for (Spellbook spellbook : spellbookRepository.findAll()) {
            if (weaponRepository.findByAlias(spellbook.getName()) == null) {
                allWeapon.add(new SpellbookAdapter(spellbook));
            }
        }

        for (Bow bow : bowRepository.findAll()) {
            if (weaponRepository.findByAlias(bow.getName()) == null) {
                allWeapon.add(new BowAdapter(bow));
            }
        }

        return allWeapon;
    }

    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon;
        String attack;
        String myLog;

        if (attackType == 0) {
            attack = "normal attack";
        } else {
            attack = "charge attack";
        }

        if (weaponRepository.findByAlias(weaponName) != null) {
            weapon = weaponRepository.findByAlias(weaponName);

        } else if (bowRepository.findByAlias(weaponName) != null) {
            Bow bow = bowRepository.findByAlias(weaponName);
            weapon = new BowAdapter(bow);

        } else if (spellbookRepository.findByAlias(weaponName) != null) {
            Spellbook spellbook = spellbookRepository.findByAlias(weaponName);
            weapon = new SpellbookAdapter(spellbook);

        } else {
            return;
        }
        myLog = String.format("%s attacked with %s (%s): ",
                weapon.getHolderName(),
                weapon.getName(),
                attack);

        if (attack.equals("normal attack")) {
            myLog += weapon.normalAttack();
        } else if (attack.equals("charge attack")) {
            myLog += weapon.chargedAttack();
        }

        logRepository.addLog(myLog);
        weaponRepository.save(weapon);
    }

    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
