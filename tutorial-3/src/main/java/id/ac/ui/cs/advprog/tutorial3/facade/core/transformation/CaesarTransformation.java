package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class CaesarTransformation {
    private int shift;

    public CaesarTransformation(int shift){
        this.shift = shift;
    }

    public CaesarTransformation(){
        this(1);
    }

    public Spell encode(Spell spell) {
        return process(spell, true);
    }

    public Spell decode(Spell spell) {
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode) {
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int codexSize = codex.getCharSize();
        String result = "";

        int shiftMove = encode ? shift : shift * -1;

        for (char x : text.toCharArray()) {
            int charIdx = codex.getIndex(x);
            int newCharIdx = charIdx + shiftMove;
            newCharIdx = newCharIdx < 0 ? codexSize + newCharIdx : newCharIdx % codexSize;

            result += codex.getChar(newCharIdx);
        }

        return new Spell(result, spell.getCodex());
    }
}
